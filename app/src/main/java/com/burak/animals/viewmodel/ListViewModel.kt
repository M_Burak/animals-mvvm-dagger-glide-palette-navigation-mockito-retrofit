package com.burak.animals.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.burak.animals.di.AppModule
import com.burak.animals.di.DaggerAnimalComponent
import com.burak.animals.di.DaggerViewModelComponent
import com.burak.animals.model.Animal
import com.burak.animals.model.AnimalsService
import com.burak.animals.model.ApiKey
import com.burak.animals.util.SharedPreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListViewModel(app: Application) : AndroidViewModel(app) {

    constructor(application: Application, test: Boolean = true): this(application){
        injected = true
    }

    val animals by lazy { MutableLiveData<List<Animal>>() }
    val loadError by lazy { MutableLiveData<Boolean>() }
    val loading by lazy { MutableLiveData<Boolean>() }

    @Inject
    lateinit var api: AnimalsService
    @Inject
    lateinit var prefs: SharedPreferencesHelper
    private var invalidApiKey = false

    private val disposable = CompositeDisposable()

    private var injected = false
    fun inject() {
        if(!injected){
            DaggerViewModelComponent.builder()
                .appModule(AppModule((getApplication())))
                .build()
                .injectListViewModel(this)
        }
    }

    fun refresh() {
        inject()
        loading.value = true
        invalidApiKey = false
        val key = prefs.getApiKey()
        if (key.isNullOrEmpty())
            getKey()
        else
            getAnimals(key)
    }

    fun getKey() {
        disposable.add(
            api.getKey()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ApiKey>() {
                    override fun onSuccess(key: ApiKey) {
                        if (key.key.isNullOrEmpty()) {
                            loadError.value = true
                            loading.value = false
                        } else {
                            prefs.saveApiKey(key.key)
                            getAnimals(key.key)
                        }
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        loading.value = false
                        loadError.value = true
                    }

                })
        )
    }


    private fun getAnimals(key: String) {

        disposable.add(
            api.getAnimals(key)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Animal>>() {
                    override fun onSuccess(list: List<Animal>) {
                        animals.value = list
                        loadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        if (!invalidApiKey) {
                            invalidApiKey = true
                            getKey()
                        } else {
                            animals.value = null
                            loadError.value = true
                            loading.value = false
                        }
                    }

                })
        )

    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}