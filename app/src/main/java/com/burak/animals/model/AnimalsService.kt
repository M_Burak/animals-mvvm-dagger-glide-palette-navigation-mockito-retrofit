package com.burak.animals.model

import com.burak.animals.di.DaggerAnimalComponent
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject


class AnimalsService {

    @Inject
    lateinit var api:AnimalApi

    init {
        DaggerAnimalComponent.create().inject(this)
    }

    fun getKey(): Single<ApiKey> {
        return api.getApiKey()
    }

    fun getAnimals(key: String): Single<List<Animal>> {
        return api.getAnimals(key)
    }
}