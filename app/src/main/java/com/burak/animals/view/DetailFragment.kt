package com.burak.animals.view


import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

import com.burak.animals.R
import com.burak.animals.model.Animal
import com.burak.countries.util.getProgressDrawable
import com.burak.countries.util.loadImage
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_list2.*

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {

    var animal: Animal? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            animal = DetailFragmentArgs.fromBundle(it).animal
        }

        (activity as MainActivity).supportActionBar?.title = "Animals"
        context?.let {
            ivAnimal.loadImage(animal?.imageUrl, getProgressDrawable(it))
        }

        tvDiet.text = animal?.diet
        tvLifespan.text = animal?.lifeSpan
        tvName.text = animal?.name
        tvLocation.text = animal?.location

        animal?.imageUrl?.let {
            setupBGColor(it)
        }
    }

    private fun setupBGColor(url: String){
        Glide.with(this)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>(){
                override fun onLoadCleared(placeholder: Drawable?) {

                }

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    Palette.from(resource)
                        .generate(){palette ->
                            val intColor = palette?.lightMutedSwatch?.rgb ?: palette?.darkVibrantSwatch?.rgb ?:0
                            llDetails.setBackgroundColor(intColor)
                        }
                }

            })
    }
}
