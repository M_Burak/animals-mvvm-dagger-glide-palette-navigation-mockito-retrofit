package com.burak.animals.view

import android.renderscript.AllocationAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.burak.animals.R
import com.burak.animals.model.Animal
import com.burak.countries.util.getProgressDrawable
import com.burak.countries.util.loadImage
import kotlinx.android.synthetic.main.item_animal.view.*

class AnimalListAdapter (var animals: ArrayList<Animal>) :
    RecyclerView.Adapter<AnimalListAdapter.AnimalViewHolder>() {
    fun updateAnimalList(newAnimals: List<Animal>) {
        animals.clear()
        animals.addAll(newAnimals)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AnimalViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_animal, parent, false)
    )

    override fun getItemCount() = animals.size

    override fun onBindViewHolder(holder: AnimalViewHolder, position: Int) {
        holder.bind(animals[position])
        holder.itemView.animalLayout.setOnClickListener {

            val action = ListFragmentDirections.actionGoToDetail(animals[position])
            Navigation.findNavController(holder.itemView).navigate(action)
        }
    }

    class AnimalViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val animalName = view.animalName
        private val animalPhoto = view.ivAnimal

        private val progressDrawable = getProgressDrawable(view.context)

        fun bind(animal: Animal) {
            animalName.text = animal.name
            animalPhoto.loadImage(animal.imageUrl,progressDrawable)
        }
    }
}