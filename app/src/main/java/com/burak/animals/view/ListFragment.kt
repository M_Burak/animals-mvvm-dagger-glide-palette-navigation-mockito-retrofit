package com.burak.animals.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.ListFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

import com.burak.animals.R
import com.burak.animals.model.Animal
import com.burak.animals.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.fragment_list2.*

/**
 * A simple [Fragment] subclass.
 */
class ListFragment : Fragment() {

    private lateinit var viewModel: ListViewModel
    private val listAdapter = AnimalListAdapter(arrayListOf())
    private val animalListDataObserver = Observer<List<Animal>> { list ->
        list?.let {
            rwAnimalList.visibility = View.VISIBLE
            listAdapter.updateAnimalList(it)
        }
    }

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        pBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            tvError.visibility = View.GONE
            rwAnimalList.visibility = View.GONE
        }

    }

    private val loadErrorLiveDataObserver = Observer<Boolean> { loadingError ->
        tvError.visibility = if (loadingError) View.VISIBLE else View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        getActivity()?.setTitle(getString(R.string.appTitle));
        viewModel.animals.observe(this, animalListDataObserver)
        viewModel.loading.observe(this, loadingLiveDataObserver)
        viewModel.loadError.observe(this, loadErrorLiveDataObserver)
        viewModel.refresh()
        rwAnimalList.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = listAdapter
        }

        (activity as MainActivity).supportActionBar?.title = "Animals"

        swRefresh.setOnRefreshListener {
            swRefresh.isRefreshing = false
            viewModel.refresh()
        }

    }

}
