package com.burak.animals.di

import com.burak.animals.model.AnimalsService
import com.burak.animals.viewmodel.ListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules=[ApiModule::class,PrefsModule::class,AppModule::class])
interface ViewModelComponent {
    fun injectListViewModel(viewModel: ListViewModel)
}