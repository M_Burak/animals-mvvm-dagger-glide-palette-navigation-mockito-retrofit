package com.burak.animals.di

import android.app.Application
import com.burak.animals.util.SharedPreferencesHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class PrefsModule {

    @Provides

    @Singleton
    open fun providePreferences(app: Application): SharedPreferencesHelper {
        return SharedPreferencesHelper(app)
    }
}