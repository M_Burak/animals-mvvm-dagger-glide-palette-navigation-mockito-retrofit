package com.burak.animals.di

import com.burak.animals.model.AnimalsService
import com.burak.animals.viewmodel.ListViewModel
import dagger.Component

@Component(modules=[ApiModule::class])
interface AnimalComponent {

    fun inject(service: AnimalsService)

}