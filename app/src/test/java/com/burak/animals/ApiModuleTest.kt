package com.burak.animals

import android.app.Application
import com.burak.animals.di.ApiModule
import com.burak.animals.di.PrefsModule
import com.burak.animals.model.AnimalsService
import com.burak.animals.util.SharedPreferencesHelper

class ApiModuleTest(val mockService: AnimalsService): ApiModule() {
    override fun providesAnimalService() : AnimalsService{
        return mockService
    }
}