package com.burak.animals


import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.burak.animals.di.AppModule
import com.burak.animals.di.DaggerViewModelComponent
import com.burak.animals.model.Animal
import com.burak.animals.model.AnimalsService
import com.burak.animals.model.ApiKey
import com.burak.animals.util.SharedPreferencesHelper
import com.burak.animals.viewmodel.ListViewModel
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor

import java.util.concurrent.TimeUnit

class ListViewModelTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @Mock
    lateinit var prefs: SharedPreferencesHelper

    @Mock
    lateinit var animalsService: AnimalsService

    val app = Mockito.mock(Application::class.java)


    var listViewModel = ListViewModel(app, true)

    private var testSingle: Single<List<Animal>>? = null

    private val key = "key"

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        DaggerViewModelComponent.builder()
            .appModule(AppModule(app))
            .apiModule(ApiModuleTest(animalsService))
            .prefsModule(PrefsModuleTest(prefs))
            .build()
            .injectListViewModel(listViewModel)
    }

    @Test
    fun getAnimalsSuccess() {

        Mockito.`when`(prefs.getApiKey()).thenReturn(key)
        val animal = Animal("dog", null, null, null, null, null, null)
        val animalList = arrayListOf(animal)
        testSingle = Single.just(animalList);

        `when`(animalsService.getAnimals(key)).thenReturn(testSingle)
        listViewModel.refresh()

        Assert.assertEquals(1, listViewModel.animals.value?.size)
        Assert.assertEquals(false, listViewModel.loadError.value)
        Assert.assertEquals(false, listViewModel.loading.value)

    }

    @Test
    fun getAnimalsFail() {
        Mockito.`when`(prefs.getApiKey()).thenReturn(key)
        testSingle = Single.error<List<Animal>>(Throwable())
        val keySingle = Single.just(ApiKey("ok", key))


        `when`(animalsService.getAnimals(key)).thenReturn(testSingle)
        `when`(animalsService.getKey()).thenReturn(keySingle)
        listViewModel.refresh()

        Assert.assertEquals(true, listViewModel.loadError.value)
        Assert.assertEquals(false, listViewModel.loading.value)

    }

    @Before
    fun setUpRxSchedulers() {
        val immediate = object : Scheduler() {

            override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Scheduler.Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() }, true)
            }
        }

        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
    }
}
