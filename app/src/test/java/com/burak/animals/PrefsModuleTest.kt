package com.burak.animals

import android.app.Application
import com.burak.animals.di.PrefsModule
import com.burak.animals.util.SharedPreferencesHelper

class PrefsModuleTest(val mockPrefs: SharedPreferencesHelper): PrefsModule() {
    override fun providePreferences(app: Application) : SharedPreferencesHelper{
        return mockPrefs
    }
}